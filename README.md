# dockerbot-prometheus

Docker-compose configuration for running prometheus stack at home.


First, clone this repo.

Then, create a ```.env``` file like this

```
GF_SECURITY_ADMIN_PASSWORD=<YOURSECUREPASWORD>
```

Finally, run:


```
docker-compose up -d
```